/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Ingredients;
import Model.Product;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Stockdao implements DaoInterface<Ingredients> {

    @Override
    public int add(Ingredients object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        //process here
        try {
            String sql = "INSERT INTO INGREDIENTS (NAME, AMOUNT) VALUES (?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getAmount());
            int row = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Ingredients> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        //process here
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM INGREDIENTS ;");
            while (rs.next()) {
                int id = rs.getInt("INGRE_ID");
                String name = rs.getString("NAME");
                double price = rs.getDouble("PRICE");
                int amount = rs.getInt("AMOUNT");
                Ingredients ingre = new Ingredients(id, name, price, amount);
                list.add(ingre);
            }
            rs.close();
            stmt.close();
            
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Ingredients get(int id) {
         Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        //process here
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM INGREDIENTS WHERE INGRE_ID=" + id);
            if (rs.next()) {
                int pid = rs.getInt("INGRE_ID");
                String name = rs.getString("NAME");
                int amount = rs.getInt("AMOUNT");
                Ingredients ingre = new Ingredients(pid, name, amount);
                return ingre;
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
            System.out.println("+++++++");
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con=db.getConnection();
          int row=0;
        //process here
        try {
            String sql = "DELETE FROM INGREDIENTS  WHERE INGRE_ID = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Ingredients object) {
        Connection con = null;
        Database db = Database.getInstance();
        con=db.getConnection();
        int row=0;
        //process here
        try {
            String sql = "UPDATE INGREDIENTS SET AMOUNT = ? WHERE INGRE_ID = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, object.getAmount());
            stmt.setInt(2, object.getIngreID());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
        return row;
    }
    public static void main(String[] args) {
        Stockdao  dao =new  Stockdao();
        System.out.println(dao.get(2));
        Ingredients in  = new Ingredients(26,"TestStock",20,28);
        dao.update(in);
        
    }
    


}
