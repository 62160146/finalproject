/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import Model.Attendance;
import Model.AttendanceDetail;
import Model.Employee;
import Model.Product;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin02
 */
public class AttendanceDao implements DaoInterface<Attendance> {

    @Override
    public int add(Attendance object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO ATTANDANCE (AT_ID,DATE) VALUES (?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
//            stmt.setDate(2,object.getDate() );
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (AttendanceDetail r : object.getAttendanceDetail()) {
                String sqlDetail = "INSERT INTO ATTENDANCE_DETAIL (EMP_ID,AT_ID,TIME_IN,TIME_OUT)VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getEmpid().getId());
                stmtDetail.setInt(2, r.getAtdid().getId());
                stmtDetail.setString(3, r.getTimein());
                stmtDetail.setString(4, r.getTimeout());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
            System.out.println("Error ka add");
        } catch (NullPointerException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.out.println("Error ka null in add");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Attendance> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM ATTENDANCE;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("AT_ID");
                String date = result.getString("DATE");
                Attendance at = new Attendance(id, date);
                list.add(at);
//                System.out.println("ID " + result.getInt("AT_ID"));
//                System.out.println("DATE " + result.getString("Date"));
            }
        } catch (SQLException ex) {
            System.out.println("ERROR      " + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Attendance get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT AT_ID,DATE FROM ATTENDANCE WHERE AT_ID=" + id;
            Statement stmts = conn.createStatement();
            ResultSet resultt = stmts.executeQuery(sql);
            if (resultt.next()) {
                int aid = resultt.getInt("AT_ID");
                String date = resultt.getString("DATE");
                Attendance att = new Attendance(aid, date);
                getAttendanceDetail(conn, id, att);
                return att;
            }
        } catch (SQLException ex) {
            System.out.println("Error ka getId");
        }
        db.close();
        return null;
    }
    private void getAttendanceDetail(Connection con, int id, Attendance att) throws SQLException {
        try {
            String sqlDetail = "SELECT ATDETAIL_ID,\n"
                    + "       EMP_ID,\n"
                    + "       A.AT_ID as AT_ID,\n"
                    + "       A.DATE as DATE,\n"
                    + "       TIME_IN,\n"
                    + "       TIME_OUT\n"
                    + "  FROM ATTENDANCE_DETAIL AD, ATTENDANCE A\n"
                    + "  WHERE A.AT_ID=? and AD.AT_ID = A.AT_ID;";
            PreparedStatement stmtDetail = con.prepareStatement(sqlDetail);
            stmtDetail.setInt(1, id);
            ResultSet rsDetail = stmtDetail.executeQuery();
            ArrayList<AttendanceDetail> atd = new ArrayList<>();
            while (rsDetail.next()) {
                int idd = rsDetail.getInt("ATDETAIL_ID");
                int empID = rsDetail.getInt("EMP_ID");
                int atID = rsDetail.getInt("AT_ID");
                String date = rsDetail.getString("DATE");
                String timein = rsDetail.getString("TIME_IN");
                String timeout = rsDetail.getString("TIME_OUT");
                Employee employee = new Employee(empID);
                AttendanceDetail x =new AttendanceDetail(idd,employee,att,timein,timeout);
                atd.add(x);
//                receipt.addReceiptDetail(idd, product, quantity, price);
            }
            att.addAttendanceDetail(atd);
        } catch (NullPointerException e) {
            System.out.println("Error HA " +e.getMessage());
        }
    }

    @Override
    public int delete(int id) {
        return 0;
    }

    @Override
    public int update(Attendance object) {
        return 0;
    }

    public static void main(String[] args) {
//        AttendanceDao dao = new AttendanceDao();
//        System.out.println("GetAll " + dao.getAll());
        AttendanceDao dao = new AttendanceDao();
        System.out.println("Get All " + dao.getAll());
        System.out.println("Get id" + dao.get(1));

    }
}
