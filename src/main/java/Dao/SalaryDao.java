/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Employee;
import Model.SalaryPayment;
import Model.SalaryPaymentDetail;
import Model.User;
import database.Database;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER01
 */
public class SalaryDao implements DaoInterface<SalaryPayment> {

    @Override
    public int add(SalaryPayment object) {
        java.sql.Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
//insert
        try {
            String sql = "INSERT INTO SALARYPAYMENT(SALARY_ID, DATE_TIME)"
                    + "VALUES(?, ?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
            stmt.setDate(2, (Date) object.getDateTime());
            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList getAll() {
        ArrayList list = new ArrayList();
        java.sql.Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
//select
        try {
            String sql = "SELECT SALARY_ID, DATE_TIME\n"
                    + " FROM SALARYPAYMENT;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int salaryId = result.getInt("SALARY_ID");
                java.util.Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("DATE_TIME"));
                SalaryPayment salaryPayment = new SalaryPayment(salaryId, date);
                list.add(salaryPayment);
                System.out.println(list);
            }
        } catch (SQLException ex) {
            System.out.println("Cannot getAll() " + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(SalaryDao.class.getName()).log(Level.SEVERE, null, ex);
        }db.close();
        return list;
    }
    ///////////////////////////////////
    public ArrayList getAllSalary() {
        ArrayList list = new ArrayList();
        java.sql.Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
//select
        try {
            String sql = "SELECT s.DATE_TIME as DATETIME, e.EMP_ID as EMP_ID, \n"
                    +"e.NAME as NAME, TOTAL_HOURS, TOTALSALARY, \n"
                    +"sd.STATUS as sdSTA, s.SALARY_ID as SALARY_ID\n" 
                    +"FROM SALARYPAYMENT s, EMPLOYEE e, SALARYPAYMENT_DETAIL sd\n" 
                    +"WHERE sd.SALARY_ID = s.SALARY_ID AND e.EMP_ID = sd.EMP_ID;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int salaryId = result.getInt("SALARY_ID");
                java.util.Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("DATE_TIME"));
                
                SalaryPayment salaryPayment = new SalaryPayment(salaryId, date);
                list.add(salaryPayment);
                //System.out.println(list);
            }
        } catch (SQLException ex) {
            System.out.println("Cannot getAll() " + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(SalaryDao.class.getName()).log(Level.SEVERE, null, ex);
        }db.close();
        return list;
    }
    
    

    @Override
    public SalaryPayment get(int id) {
        java.sql.Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
//select
        try {
            String sql = "SELECT s.SALARY_ID as SALARYID, s.DATE_TIME as DATE_TIME, "
                    +"e.NAME as NAME, TOTAL_HOURS, TOTALSALARY, sd.STATUS as sdSTA\n"
                    + "FROM SALARYPAYMENT s, SALARYPAYMENT_DETAIL sd,EMPLOYEE e WHERE SALARY_ID = ?"
                    +";";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int salaryId = result.getInt("SALARYID");
                Date date = result.getDate("DATE_TIME");
                String name = result.getString("NAME");
                int totalHours = result.getInt("TOTAL_HOURS");
                double totalSalary = result.getDouble("TOTALSALARY");
                boolean status = result.getBoolean("sdTA");
                SalaryPayment salaryPayment = new SalaryPayment(salaryId, date);

                getSalaryPaymentDetail(conn, id, salaryPayment);

                return salaryPayment;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select salary payment id " + id + "!!" + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(SalaryDao.class.getName()).log(Level.SEVERE, null, ex);
        }db.close();
        return null;
    }

    private void getSalaryPaymentDetail(Connection conn, int id, SalaryPayment salaryPayment) throws SQLException, ParseException {
        String sqlDetail = "SELECT SALARYDETAIL_ID,\n"
                + "                       e.EMP_ID as EMP_ID, e.NAME as NAME,\n"
                + "                       u.USER_ID as USER_ID,\n"
                + "                       u.PASSWORD as PASSWORD,\n"
                + "                       u.USERNAME as USERNAME,\n"
                + "                        e.STATUS as EMPST,\n"
                + "                       s.SALARY_ID as SALARY_ID,\n"
                + "                       s.DATE_TIME as DATE_TIME,\n"
                + "                       TOTALSALARY,\n"
                + "                       STATUS,\n"
                + "                       TOTAL_HOURS\n"
                + "                  FROM SALARYPAYMENT_DETAIL, EMPLOYEE e, USER u, SALARYPAYMENT s;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while (resultDetail.next()) {
            int salaryDetailId = resultDetail.getInt("SALARYDETAIL_ID");
            int empId = resultDetail.getInt("EMP_ID");
            String name = resultDetail.getString("NAME");
            int userId = resultDetail.getInt("USER_ID");
            String empStatus = resultDetail.getString("EMPST");
            String userName = resultDetail.getString("USERNAME");
            String password = resultDetail.getString("PASSWORD");
            double totalSalary = resultDetail.getDouble("TOTALSALARY");
            boolean status = resultDetail.getBoolean("STATUS");
            int totalHours = resultDetail.getInt("TOTAL_HOURS");
            int salaryId = resultDetail.getInt("SALARY_ID");
            java.util.Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(resultDetail.getString("DATE_TIME"));

            SalaryPaymentDetail salaryPaymentDetail = new SalaryPaymentDetail(salaryDetailId,
                    new Employee(empId, name, new User(userId, userName, password), empStatus),
                    new SalaryPayment(salaryId, date), totalSalary, status, totalHours);

            //Product product = new Product(productId, productName, productPrice);
            //receipt.addReceiptDetail(receiptId, product, amount, price);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
//delete
        try {
            String sql = "DELETE FROM SALARYPAYMENT\n"
                    + "      WHERE SALARY_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete salary payment id" + id + "!!");
        }db.close();
        return row;
    }

    @Override
    public int update(SalaryPayment object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void main(String[] args) {
        SalaryDao dao = new SalaryDao();
        System.out.println("Get All : " + dao.getAll());
        System.out.println("Get (i) : " + dao.get(1));
        //System.out.println("Get Detail : "+dao.getSalaryPaymentDetail(conn, 0, salaryPayment));
    }

}
