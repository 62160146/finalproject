/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Customer;
import Model.Employee;
import Model.Product;
import Model.Receipt;
import Model.ReceiptDetail;
import Model.User;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class Receiptdao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        //process here
        try {
            String sql = "INSERT INTO RECEIPT (EMP_ID, CUS_ID, SUB_TOTAL,DISCOUNT,TOTAL,USED_POINT,CASH, EXCHANGE) VALUES (?, ?,?,?,?,?,?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, object.getEmpID().getId());
            stmt.setInt(2, object.getCusID().getId());
            stmt.setDouble(3, object.getSubtotal());
            stmt.setDouble(4, object.getDiscount());
            stmt.setDouble(5, object.getTotal());
            stmt.setInt(6, object.getUsed_point());
            stmt.setDouble(7, object.getCash());
            stmt.setDouble(8, object.getExchage());
            int row = stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
                object.setReceiptID(id);
            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO RECEIPT_DETAIL (RECEIPT_ID, PRODUCT_ID,QUANTITY,PRICE,TOTAL_PRICE)\n"
                        + "                           VALUES (?,?, ?, ?,? );";
                PreparedStatement stmtDetail = con.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getReceiptID().getReceiptID());
                stmtDetail.setInt(2, r.getProductID().getId());
                stmtDetail.setInt(3, r.getQuantity());
                stmtDetail.setDouble(4, r.getPrice());
                stmtDetail.setDouble(5, r.getTotalprice());
                int rowDetail = stmtDetail.executeUpdate();

                ResultSet rsDetail = stmt.getGeneratedKeys();

                if (rsDetail.next()) {
                    id = rsDetail.getInt(1);
                    r.getReceiptdetailID();
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
            System.out.println("Plaiifah is very adorrrr");
        } catch (NullPointerException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.out.println("Plaiifah wanna die now");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        //process here
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM RECEIPT");
            while (rs.next()) {
                int receiptID = rs.getInt("RECEIPT_ID");
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("TIME"));
                int empId = rs.getInt("EMP_ID");
                double total = rs.getDouble("TOTAL");
                double subtotal = rs.getDouble("SUB_TOTAL");
                double cash = rs.getDouble("CASH");
                double exchange = rs.getDouble("EXCHANGE");
                double discount = rs.getDouble("DISCOUNT");
                int cusID = rs.getInt("CUS_ID");
                int usedpoint = rs.getInt("USED_POINT");
                Receipt receipt = new Receipt(receiptID, date, new Employee(empId), total, subtotal, cash, exchange,
                        discount, new Customer(cusID),usedpoint);
                list.add(receipt);
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(Receiptdao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        //process here 
        try {
            String sql = "SELECT r.RECEIPT_ID as RECEIPT_ID ,\n"
                    + "                    r.TIME as TIME, \n"
                    + "                    e.EMP_ID as EMP_ID,\n"
                    + "                    r.SUB_TOTAL as SUB_TOTAL,\n"
                    + "                    r.CASH as  CASH,\n"
                    + "                    r.EXCHANGE as EXCHANGE,\n"
                    + "                    r.DISCOUNT as DISCOUNT,\n"
                    + "                    c.CUS_ID as CUS_ID,\n"
                    + "                     c.NAME as NAME,\n"
                    + "                    c.TOTAL_POINT as TOTAL_POINT, \n"
                    + "                    r.TOTAL as TOTAL, \n"
                    + "                   r.USED_POINT as USED_POINT\n"
                    + "                   FROM 	RECEIPT r, CUSTOMER c, EMPLOYEE e\n"
                    + "                   WHERE RECEIPT_ID = ? ;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int receiptID = rs.getInt("RECEIPT_ID");
                Date time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("TIME"));
                int empId = rs.getInt("EMP_ID");
                double subtotal = rs.getDouble("SUB_TOTAL");
                double cash = rs.getDouble("CASH");
                double exchange = rs.getDouble("EXCHANGE");
                double discount = rs.getDouble("DISCOUNT");
                int cusID = rs.getInt("CUS_ID");
                String name = rs.getString("NAME");
                int totalpoint = rs.getInt("TOTAL_POINT");
                double total = rs.getDouble("TOTAL");
                int usedpoint = rs.getInt("USED_POINT");
                Receipt receipt = new Receipt(receiptID, time, new Employee(empId), total, subtotal, cash, exchange,
                        discount, new Customer(cusID, name, totalpoint));
                getReceiptDetail(con, id, receipt);
                return receipt;
            }
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(Receiptdao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    private void getReceiptDetail(Connection con, int id, Receipt receipt) throws SQLException {
        try {
            String sqlDetail = "SELECT rd.RECEIPTDETAIL_ID as ID,\n"
                     + "      p.PRODUCT_ID as PRODUCT_ID,\n"
                    + "       p.NAME as PRODUCT_NAME,\n"
                    + "       rd.QUANTITY as QUANTITY,\n"
                    + "       rd.PRICE as PRICE,\n"
                    + "       rd.TOTAL_PRICE as TOTAL_PRICE\n"
                    + "  FROM RECEIPT_DETAIL rd, PRODUCT p\n"
                    + "  WHERE RECEIPT_ID = ? AND rd.PRODUCT_ID = p.PRODUCT_ID;";
            PreparedStatement stmtDetail = con.prepareStatement(sqlDetail);
            stmtDetail.setInt(1, id);
            ResultSet rsDetail = stmtDetail.executeQuery();
            ArrayList<ReceiptDetail> rcd = new ArrayList<>();
            while (rsDetail.next()) {
                int idd = rsDetail.getInt("ID");
                int productId = rsDetail.getInt("PRODUCT_ID");
                String productName = rsDetail.getString("PRODUCT_NAME");
                int quantity = rsDetail.getInt("QUANTITY");
                double price = rsDetail.getDouble("PRICE");
                int totalprice = rsDetail.getInt("TOTAL_PRICE");
                Product product = new Product(productId, productName, price);
                ReceiptDetail x =new ReceiptDetail(idd,receipt,product,quantity,price,totalprice);
                rcd.add(x);
//                receipt.addReceiptDetail(idd, product, quantity, price);
            }
            receipt.addReceiptDetail(rcd);
        } catch (NullPointerException e) {
            System.out.println("Error get : " +e.getMessage());
        }
    }

    @Override
    public int delete(int id) {
        return 0;
    }

    @Override
    public int update(Receipt object) {
        return 0;
    }

    public static void main(String[] args) {
        Receiptdao dao = new Receiptdao();
//        User user = new User(1,"Mindnaevis","1234");
//        Employee emp = new Employee(1,"Natcha Sangkaew" ,user,"S");
//        Customer cus = new Customer(26,"A","0800000000",2000,0,0);
//        ArrayList<ReceiptDetail> list = new ArrayList<>();
//        Receipt r = new Receipt(-1,emp,cus,500,50,450,500,50);
//        Product p = new Product(6,"Hot Milk",50	);
//        list.add(new ReceiptDetail(-1,r,p,10,p.getPrice(),(10*p.getPrice())));
//        dao.add(r);
        System.out.println("Test get(i) dao = " + dao.get(1));

    }
}
