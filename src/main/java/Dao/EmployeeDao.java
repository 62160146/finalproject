/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Employee;
import Model.User;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class EmployeeDao implements DaoInterface<Employee> {

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Employee (name,user_id,status)VALUES (?,?,?) ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getUserId().getId());
            stmt.setString(3, String.valueOf(object.getStatus()));
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT EMP_ID,\n"
                    + "       NAME,\n"
                    + "       USER_ID,\n"
                    + "       STATUS\n"
                    + "  FROM EMPLOYEE;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("EMP_ID");
                String name = result.getString("NAME");
                int userId = result.getInt("USER_ID");
                String status = result.getString("STATUS");
                Employee em = new Employee(id, name, new User(userId), status);
                list.add(em);

            }
        } catch (SQLException ex) {
            System.out.println("ERROR 404 NOT FOUND!!!!!!!!!!!" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT EMP_ID,NAME,USER_ID,STATUS FROM EMPLOYEE WHERE EMP_ID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int emid = result.getInt("EMP_ID");
                String name = result.getString("NAME");
                int userId = result.getInt("USER_ID");
                String status = result.getString("STATUS");
                Employee em = new Employee(emid, name, new User(userId), status);
                return em;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR 404 NOT FOUND " + ex.getMessage());
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM EMPLOYEE WHERE EMP_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("ERROR");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = -1;
        try {
            String sql = "UPDATE EMPLOYEE\n"
                    + "    SET NAME = ?,\n"
                    + "       USER_ID = ?,\n"
                    + "       STATUS = ?\n"
                    + "       WHERE EMP_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getUserId().getId());
            stmt.setString(3, object.getStatus());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("ERROR Modpow cuteies" + ex.getMessage());
        }
        db.close();
        return row;
    }
    public static void main(String[] args) {
        EmployeeDao dao = new EmployeeDao();
        System.out.println(dao.getAll());
//        Employee em = new Employee(13,"Fahjajak",new User(5),"S");
//        System.out.println("Update na ka");
//        dao.update(em);
        System.out.println(dao.getAll());
    }
}
