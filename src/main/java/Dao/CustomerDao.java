/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Customer;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ii drunkboy
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;

        try {
            String sql = "INSERT INTO CUSTOMER (NAME, TEL) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT CUS_ID, NAME, TEL, TOTAL_POINT, TOTAL_RECEIPT, TOTAL_DISCOUNT FROM CUSTOMER";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("CUS_ID");
                String name = result.getString("NAME");
                String tel = result.getString("TEL");
                int TotalPoint = result.getInt("TOTAL_POINT");
                int TOtalReceipt = result.getInt("TOTAL_RECEIPT");
                int TOtalDiscount = result.getInt("TOTAL_DISCOUNT");
                Customer customer = new Customer(id, name, tel, TotalPoint, TOtalReceipt, TOtalDiscount);
                list.add(customer);
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT CUS_ID, NAME, TEL, TOTAL_POINT, TOTAL_RECEIPT, TOTAL_DISCOUNT FROM CUSTOMER WHERE CUS_ID ="+ id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int cid = result.getInt("CUS_ID");
                String name = result.getString("NAME");
                String tel = result.getString("TEL");
                int TotalPoint = result.getInt("TOTAL_POINT");
                int TOtalReceipt = result.getInt("TOTAL_RECEIPT");
                int TOtalDiscount = result.getInt("TOTAL_DISCOUNT");
                Customer customer = new Customer(cid, name, tel, TotalPoint, TOtalReceipt, TOtalDiscount);
                return customer;
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM CUSTOMER WHERE CUS_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE CUSTOMER SET TOTAL_POINT = ?, TOTAL_RECEIPT = ?, TOTAL_DISCOUNT = ? WHERE CUS_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getTotalPoint());
            stmt.setInt(2, object.getTotalReceipt());
            stmt.setDouble(3, object.getTotalDiscount());
            stmt.setInt(4, object.getId());
            System.out.println("Affect row " + row);
            //System.out.println("Test = "+object.getId());
            row = stmt.executeUpdate();      
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
        return row;
    }
     public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
//        System.out.println(dao.getAll());
//        System.out.println(dao.get(19));
//        Customer cus = new Customer(23,"A","0800000000",1100,4,0);
//        dao.update(cus);
    }
}

