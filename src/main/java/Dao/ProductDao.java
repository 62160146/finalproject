/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Ingredients;
import Model.Product;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin02
 */
public class ProductDao implements DaoInterface<Product>{

    @Override
    public int add(Product object) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO PRODUCT (NAME,PRICE,INGRE_ID)VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setDouble(2,object.getPrice());
            stmt.setInt(3,object.getIngredients().getIngreID());
            int row =stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                id = result.getInt(1);
            }
           
        } catch (SQLException ex) {
            System.err.println("Error ka add");
        }
        db.close();
        return id;
    }

    

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT PRODUCT_ID,NAME,PRICE,IMAGE,INGRE_ID FROM PRODUCT";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("PRODUCT_ID");
                String name = result.getString("NAME");
                double price = result.getDouble("PRICE");
                String image = result.getString("IMAGE");
                int reid = result.getInt("INGRE_ID");
                
                Product product = new Product(id,name,price,image,new Ingredients(reid));
                list.add(product);
                //System.out.println(product.toString());
            }
        } catch (SQLException ex) {
            System.err.println("Error ka getAll");
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ID,NAME,PRICE,IMAGE FROM PRODUCT WHERE id=" +id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("ID");
                String name = result.getString("NAME");
                double price = result.getDouble("PRICE");
                 
                Product product = new Product(pid,name,price);
                return product;
            }
        } catch (SQLException ex) {
            System.out.println("Error ka getId");
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
         Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
       int row = 0;
        try {
            String sql = "DELETE FROM product WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row =stmt.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println("Error ka delete");
        }
       db.close();
       return row;
    }

   

    @Override
    public int update(Product object) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row=-1;
        try {
            String sql = "UPDATE PRODUCT  SET  NAME = ?, PRICE = ?  WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
             row =stmt.executeUpdate();
            
           
        } catch (SQLException ex) {
            System.out.println("Error ka update");
        }
        db.close();
        return row;
    
    }
    
    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        System.out.println("Product getAll = "+dao.getAll());
    }
    
}
