/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Dao.EmployeeDao;
import Dao.Receiptdao;
import Model.Customer;
import Model.Employee;
import Model.Product;
import Model.Receipt;
import Model.User;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author USER01
 */
public class ProductService {

    private static ArrayList<Product> productList = new ArrayList<>();
    private static Product currentProduct = null;
    private static Receipt rec = null;
    private static User user = null;
    private static Employee emp = null;
    private static Customer cus = null;

    private static double subTotal = 0, discount = 0, total = 0;
    private static int curPoint = 0, usePoint = 0;

    // Create (C)
    public static boolean addProduct(Product product) {
        productList.add(product);
        return true;
    }

    // Delete (D)
    public static boolean delProduct(Product product) {
        productList.remove(product);
        return true;
    }

    public static boolean clearProduct() {
        productList.clear();
        return true;
    }

    public static boolean delProduct(int index) {
        productList.remove(index);
        return true;
    }

    // Read (R)
    public static ArrayList<Product> getProducts() {
        return productList;
    }

    public static Product getProduct(int index) {
        return productList.get(index);
    }

    // Update (U)
    public static boolean updateProduct(int index, Product product) {
        productList.set(index, product);
        return true;
    }

    public static void printProduct() {
        System.out.println("What is in your List? =");
        for (int i = 0; i < productList.size(); i++) {
            System.out.println(productList.get(i));
        }
    }
    public static ArrayList<String> getOrder() {
        ArrayList<String> list = new ArrayList<>();
        for(Product p:productList){
            String x = p.getName()+" >> "+p.getPrice()+"(x"+p.getAmount()+") = THB"+(p.getPrice()*p.getAmount())+" ";
            list.add(x);
        }
        return list;
    }

    //--------------------------------------------------------------------------
    public static double calSubTotal() {
        subTotal = 0;
        for (int i = 0; i < productList.size(); i++) {
            subTotal += (productList.get(i).getPrice() * productList.get(i).getAmount());
        }
        return subTotal;
    }

    public static double calDiscount() {
        discount = 0;
        if (subTotal >= 500) {
            discount = (subTotal * 0.01);
            double decimal = discount - (int) discount;
            if (decimal > 0) {
                if (decimal < 0.25) {
                    discount = (int) discount + 0.25;
                } else if (decimal < 0.50) {
                    discount = (int) discount + 0.50;
                } else if (decimal > 0.50) {
                    discount = Math.ceil(discount);
                }

            }
        }
        return discount;
    }

    public static int usePoint(int Point) {
        usePoint = (Point / 100);
        total = total - (Point / 100);
        return Point / 100;
    }
    
    public static int usedPoint() {
        return usePoint*100;
    }
    

    public static int calPoint() {
        int allAmount = 0;
        for (int i = 0; i < productList.size(); i++) {
            allAmount += productList.get(i).getAmount();
        }
        curPoint = allAmount * 5;
        return curPoint;
    }

    public static double calTotal() {
        total = subTotal - discount - usePoint;
        return total;
    }

    public static double calExchange(double cash) {
        double exchange = 0;
        exchange = cash - total;
        return exchange;
    }
//    --------------------------------------------------------------------------

    public static void addReceipt(Receipt receipt) {
        rec = receipt;
    }
    public static String getCash() {
        return ""+rec.getCash();
    }
    public static String getExchange() {
        return ""+rec.getExchage();
    }
    

    public static String getQReceipt() {
        return "ORDER #" + rec.getReceiptID();
    }

    public static String getDateReceipt() {
        Receiptdao dao = new Receiptdao();
        Receipt r = dao.get(rec.getReceiptID());
        Date x = r.getDate();
        SimpleDateFormat f = new SimpleDateFormat("dd-MMMM-yyyy HH:mm:ss",new Locale("en"));
        String format = f.format(x);
        System.out.println(format);
        return format;
    }
    
    public static String getDate(int id) {
        Receiptdao dao = new Receiptdao();
        Receipt r = dao.get(id);
        Date x = r.getDate();
        SimpleDateFormat f = new SimpleDateFormat("dd-MMM-yyyy",new Locale("en"));
        String format = f.format(x);
        System.out.println(format);
        return format;
    }

//    --------------------------------------------------------------------------
    public static void addUser(User u) {
        user = u;
    }

    public static Employee getEmployee() {
        EmployeeDao dao = new EmployeeDao();
        Employee employee = dao.get(user.getId());
        return employee;
    }
//    --------------------------------------------------------------------------

    public static Customer getCustomer() {
        return cus;
    }

    public static void addCustomer(Customer customer) {
        cus = customer;
    }
//    --------------------------------------------------------------------------
    public static void refreshPrintReceipt(){
        total = 0;
        cus = null;
        usePoint = 0;
    }
}
