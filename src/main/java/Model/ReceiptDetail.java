/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author User
 */
public class ReceiptDetail {
    
    private int receiptdetailID;
    private Receipt receipt;
    private Product product;
    private int quantity;
    private double price;
    private double totalprice;

    public ReceiptDetail(int receiptdetailID, Receipt receiptID, Product productID, int quantity, double price, double totalprice) {
        this.receiptdetailID = receiptdetailID;
        this.receipt = receiptID;
        this.product = productID;
        this.quantity = quantity;
        this.price = price;
        this.totalprice = totalprice;
    }
    public ReceiptDetail(int receiptdetailID, Product productID, int quantity, double price, double totalprice) {
        this.receiptdetailID = receiptdetailID;
        this.product = productID;
        this.quantity = quantity;
        this.price = price;
        this.totalprice = totalprice;
    }

    ReceiptDetail(int id, Product product, int amount, double price, Receipt This) {
        this.receiptdetailID = id;
        this.product = this.product;
        this.quantity = amount;
        this.price = price;
        this.receipt = This;
    }

    public int getReceiptdetailID() {
        return receiptdetailID;
    }

    public void setReceiptdetailID(int receiptdetailID) {
        this.receiptdetailID = receiptdetailID;
    }

    public Receipt getReceiptID() {
        return receipt;
    }

    public void setReceiptID(Receipt receiptID) {
        this.receipt = receiptID;
    }

    public Product getProductID() {
        return product;
    }

    public void setProductID(Product productID) {
        this.product = productID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(double totalprice) {
        this.totalprice = totalprice;
    }
    
        public void addQuan(int a){
        this.quantity = this.quantity+a;  
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "receiptdetailID=" + receiptdetailID + ", receiptID=" + receipt + ", productID=" + product + ", quantity=" + quantity + ", price=" + price + ", totalprice=" + totalprice + '}';
    }

    
}
