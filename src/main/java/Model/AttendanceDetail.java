/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author admin02
 */
public class AttendanceDetail {
    int id;
    private Employee employee;
    private Attendance attendacne;
    private String timein;
    private String timeout;
    

    public AttendanceDetail(int id, Employee empid, Attendance atdid, String timein, String timeout) {
        this.id = id;
        this.employee = empid;
        this.attendacne = atdid;
        this.timein = timein;
        this.timeout = timeout;
    }
    

    public AttendanceDetail() {
    }
    
//    AttendanceDetail(int id, Employee empid, String timein, String timeout Attendance This) {
//        this.id = id; 
//        this.empid = this.empid;
//        this.timein = timein;
//        this.timeout = timeout;
//        this.atdid = This;
//    }

    @Override
    public String toString() {
        return "AttendanceDetail{" + "id=" + id + ", empid=" + employee + ", atdid=" + attendacne + ", timein=" + timein + ", timeout=" + timeout + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmpid() {
        return employee;
    }

    public void setEmpid(Employee empid) {
        this.employee = empid;
    }

    public Attendance getAtdid() {
        return attendacne;
    }

    public void setAtdid(Attendance atdid) {
        this.attendacne = atdid;
    }

    public String getTimein() {
        return timein;
    }

    public void setTimein(String timein) {
        this.timein = timein;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }
}
