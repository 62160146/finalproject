/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Dao.ProductDao;
import Dao.Stockdao;
import java.util.ArrayList;

/**
 *
 * @author Acer
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private Ingredients ingredients;
    private String image;
    private int amount = 1;

    @Override
    public String toString() {
        String x = name + " >> " + price + "(x" + amount + ") = THB" + (price * amount) + " ";
        return x;
    }

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }
    public Product(int id, String name, double price, String image,Ingredients x) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        ingredients = x;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Product(int id, String name, double price, Ingredients ingreId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.ingredients = ingredients;
    }

    public Product(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public Product(int id, String name, String password) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Ingredients getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredients ingredients) {
        this.ingredients = ingredients;
    }

    //increse amount
    public void increaseAmount() {
        amount++;
    }

    public void decreaseAmount(int x) {
        amount=amount-x;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public static ArrayList<Product> genProductList() {
        ProductDao productDao = new ProductDao();
        Stockdao sdao = new Stockdao();

        ArrayList<Product> product = productDao.getAll();
        ArrayList<Product> newproduct = new ArrayList<>();
        for(Product p:product){
            
            Ingredients ingre=sdao.get(p.ingredients.getIngreID());
            if(ingre.getAmount()>0){
                newproduct.add(p);
            }
            
        }
        
//        
        return newproduct;
    }

}
