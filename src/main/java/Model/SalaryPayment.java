/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author USER01
 */
public class SalaryPayment {
    private int id;
    private Date dateTime;
    ArrayList<SalaryPaymentDetail> salaryPaymentDetail = new ArrayList<>();

    public SalaryPayment(int id, Date dateTime) {
        this.id = id;
        this.dateTime = dateTime;
        salaryPaymentDetail = new ArrayList<>();
    }
    
    public void addSalaryPaymentDetail(int id, Employee empId, SalaryPayment salaryId,double total, boolean status, int totalHours){
//        for(int i=0; i<salaryPaymentDetail.size(); i++){
//            SalaryPaymentDetail s = salaryPaymentDetail.get(i);
//            
//        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "SalaryPayment{" + "id=" + id + ", dateTime=" + dateTime + '}';
    }
    
}
