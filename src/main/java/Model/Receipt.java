/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;
import java.util.ArrayList;

/**
 *
 * @author User
 */
public class Receipt {

    private int receiptID;
    private Date Date;
    private Employee empID;
    private double total;
    private double subtotal;
    private double cash;
    private double exchange;
    private double discount;
    private Customer cusID;
    private int used_point;
    private ArrayList<ReceiptDetail> receiptDetail = new ArrayList<>();

    public Receipt(int receiptID, Date date, Employee employee, double total, double subtotal, double cash, double exchange, double discount, Customer customer, int usedpoint) {
        this.receiptID = receiptID;
        this.Date = date;
        this.empID = employee;
        this.total = total;
        this.subtotal = subtotal;
        this.cash = cash;
        this.exchange = exchange;
        this.discount = discount;
        this.cusID = customer;
        this.used_point=usedpoint;
        receiptDetail = new ArrayList<>();
    }

    public int getUsed_point() {
        return used_point;
    }

    public Receipt(int receiptID, Date Date, Employee empID, double total, double subtotal, double cash, double exchage, double discount, Customer cusID) {
        this.receiptID = receiptID;
        this.Date = Date;
        this.empID = empID;
        this.total = total;
        this.subtotal = subtotal;
        this.cash = cash;
        this.exchange = exchage;
        this.discount = discount;
        this.cusID = cusID;
        receiptDetail = new ArrayList<>();

    }

    public Receipt(int receiptID, Employee empID, Customer cusID, double subtotal, double discount, double total, double cash, double exchage, int upoint) {
        this.receiptID = receiptID;
        this.empID = empID;
        this.total = total;
        this.subtotal = subtotal;
        this.cash = cash;
        this.exchange = exchage;
        this.discount = discount;
        this.cusID = cusID;
        this.used_point=upoint;
        receiptDetail = new ArrayList<>();

    }

    public int getReceiptID() {
        return receiptID;
    }

    public void setReceiptID(int receiptID) {
        this.receiptID = receiptID;
    }

    public Date getDate() {
        return Date;
    }

    public void setDate(Date Date) {
        this.Date = Date;
    }

    public Employee getEmpID() {
        return empID;
    }

    public void setEmpID(Employee empID) {
        this.empID = empID;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getExchage() {
        return exchange;
    }

    public void setExchage(double exchage) {
        this.exchange = exchage;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public Customer getCusID() {
        return cusID;
    }

    public void setCusID(Customer cusID) {
        this.cusID = cusID;
    }

    public ArrayList<ReceiptDetail> getReceiptDetail() {
        return receiptDetail;
    }

    @Override
    public String toString() {
        return "Receipt{" + "receiptID=" + receiptID + ", Date=" + Date + ", empID=" + empID + ", total=" + total + ", subtotal=" + subtotal + ", cash=" + cash + ", exchage=" + exchange + ", discount=" + discount + ", cusID=" + cusID + ", used_point=" + used_point + '}';
    }

    public void addReceiptDetail(ArrayList<ReceiptDetail> a) {
        receiptDetail = a;
        System.out.println("Test addReceiptDetail = "+a);
    }
    public void addReceiptDetail(int id, Product product, int amount, double price) {
        for (int row = 0; row < receiptDetail.size(); row++) {
            ReceiptDetail r = receiptDetail.get(row);
            if (r.getProductID().getId() == product.getId()) {
                r.addQuan(amount);
                return;
            }

        }
        receiptDetail.add(new ReceiptDetail(id, product, amount, price, this));
    }
}
