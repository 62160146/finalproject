/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Acer
 */
public class Employee {
    private int id;
    private String name;
    private User userId;
    private String status;

    public Employee(int id, String name, User userId, String status) {
        this.id = id;
        this.name = name;
        this.userId = userId;
        this.status = status;
    }
    public Employee(int id){
        this.id = id;
    }
    
    public Employee(){
        
    }
    
    public Employee(User userId){
        this(-1,null ,userId,null);
    }
//    public Employee(int id, String name, String status){
//        
//    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", userid=" + userId + ", status=" + status + '}';
    }

    public int lenght() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
