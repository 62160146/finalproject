/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author User
 */
public class Ingredients {
    
    private int ingreID;
    private String name;
    private double price;
    private int amount;
    
    public Ingredients(int ingreID, String name, double price, int amount) {
        this.ingreID = ingreID;
        this.name = name;
        this.price = price;
        this.amount = amount;
    }

    public Ingredients(int ingreID,String name, int amount) {
        this.ingreID = ingreID;
         this.name = name;
          this.amount = amount;
    }

    public  Ingredients(int o) {
        this.ingreID = o;
    }

    public int getIngreID() {
        return ingreID;
    }

    public void setIngreID(int ingreID) {
        this.ingreID = ingreID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Ingredients{" + "ingreID=" + ingreID + ", name=" + name + ", price=" + price + ", amount=" + amount + '}';
    }

    public void delAmount(int x) {
        amount=amount-x;
    }
    public void addAmount(int x) {
        amount=amount+x;
    }
    
}
