/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author ii drunkboy
 */
public class Customer {
    private int id;
    private String name;
    private String tel;
    private int TotalPoint;
    private int TotalReceipt;
    private double TotalDiscount;

    public Customer(int id,String name, String tel, int TotalPoint, int TotalReceipt, double TotalDiscount) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.TotalPoint = TotalPoint;
        this.TotalReceipt = TotalReceipt;
        this.TotalDiscount = TotalDiscount;
    }
    
    public Customer(){
        
    }

    public Customer(int id, String name, int TotalPoint) {
        this.id = id;
        this.name = name;
        this.TotalPoint = TotalPoint;
    }
//    public Customer(Object o, String name, int TotalPoint) {
//        
//        this.name = name;
//        this.TotalPoint = TotalPoint;
//    }
    public Customer(int id, String name,String tel) {
        this.id = id;
        this.name = name;
        this.tel = tel;
    }
    public Customer(String name, String tel) {
        this(-1,name,tel);
    }

    public Customer(int cusID) {
        this.id=cusID;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getTotalPoint() {
        return TotalPoint;
    }

    public void setTotalPoint(int TotalPoint) {
        this.TotalPoint = TotalPoint;
    }

    public int getTotalReceipt() {
        return TotalReceipt;
    }

    public void setTotalReceipt(int TotalReceipt) {
        this.TotalReceipt = TotalReceipt;
    }

    public double getTotalDiscount() {
        return TotalDiscount;
    }

    public void setTotalDiscount(double TotalDiscount) {
        this.TotalDiscount = TotalDiscount;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", TotalPoint=" + TotalPoint + ", TotalReceipt=" + TotalReceipt + ", TotalDiscount=" + TotalDiscount + '}';
    }
    
}
