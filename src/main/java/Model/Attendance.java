/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import java.util.ArrayList;
import java.sql.Date;
/**
 *
 * @author admin02
 */
public class Attendance {
     private int id;
     private String date;
     private ArrayList<AttendanceDetail>attendanceDetail=new ArrayList<>();
     

    @Override
    public String toString() {
        return "Attendance{" + "id=" + id + ", date=" + date + '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public Attendance(int id, String date) {
        this.id = id;
        this.date = date;
    }
    public ArrayList<AttendanceDetail> getAttendanceDetail() {
        return attendanceDetail;
    }
    public void addAttendanceDetail(ArrayList<AttendanceDetail> a) {
        attendanceDetail = a;
    }
    
}
