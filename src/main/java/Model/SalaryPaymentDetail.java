/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author USER01
 */
public class SalaryPaymentDetail {
    private int id;
    private Employee empId;
    private SalaryPayment salaryId;
    private double total;
    private boolean status;
    private int totalHours;
    //add
    private String name;

    public SalaryPaymentDetail(int id, Employee empId, SalaryPayment salaryID, double total, boolean status, int totalHours) {
        this.id = id;
        this.empId = empId;
        this.salaryId = salaryID;
        this.total = total;
        this.status = status;
        this.totalHours = totalHours;
    }
    public SalaryPaymentDetail(int id,String name ,Employee empId, SalaryPayment salaryID, double total, boolean status, int totalHours) {
        this.id = id;
        this.empId = empId;
        this.salaryId = salaryID;
        this.total = total;
        this.status = status;
        this.totalHours = totalHours;
        this.name = name;
    }
    public String getName(){
        return name;
    }
    
    public void setName(){
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmpId() {
        return empId;
    }

    public void setEmpId(Employee empId) {
        this.empId = empId;
    }

    public SalaryPayment getSalaryID() {
        return salaryId;
    }

    public void setSalaryID(SalaryPayment salaryID) {
        this.salaryId = salaryID;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(int totalHours) {
        this.totalHours = totalHours;
    }

    @Override
    public String toString() {
        return "SalaryPaymentDetail{" + "id=" + id + ", empId=" + empId + ", salaryID=" + salaryId + ", total=" + total + ", status=" + status + ", totalHours=" + totalHours + '}';
    }
    
}
